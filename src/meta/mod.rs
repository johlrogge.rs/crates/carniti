use im::ordset::OrdSet;
use resiter::*;
use thiserror::Error;
use vfs::VfsPath;
use vfs_tools::VfsIteratorExt;

#[derive(Error, Debug)]
pub enum MetaError {
    #[error("VFS error {0}")]
    VfsError(#[from] ::vfs::VfsError),
    #[error("IO error: {0}")]
    IOError(#[from] std::io::Error),
}

pub type MetaResult<T> = Result<T, MetaError>;

#[derive(Debug, PartialEq, Eq, Clone, PartialOrd, Ord)]
pub struct Path(String);

#[derive(Debug, PartialEq, Eq, Clone, PartialOrd, Ord)]
pub struct Document(Path);

pub struct MetaData {
    pub documents: OrdSet<Document>,
}

impl From<VfsPath> for Path {
    fn from(value: VfsPath) -> Self {
        fn walk_to_root(path: &VfsPath) -> Path {
            let current_file_name = path.filename().to_owned();
            if path.is_root() {
                Path(current_file_name)
            } else {
                Path(format!(
                    "{}/{}",
                    walk_to_root(&path.parent()).0,
                    current_file_name.to_owned()
                ))
            }
        }
        walk_to_root(&value)
    }
}

impl Document {
    pub fn load(path: VfsPath) -> MetaResult<Document> {
        Ok(Document(path.into()))
    }
}

pub fn extract_meta(root: &VfsPath) -> MetaResult<MetaData> {
    let document_paths = root.walk_dir()?.files_only().with_extension("md");
    let documents = document_paths
        .into_iter()
        .map_err(MetaError::from)
        .try_map_ok(Document::load)
        .collect::<MetaResult<OrdSet<Document>>>()?;
    MetaResult::Ok(MetaData { documents })
}

#[cfg(test)]
mod tests {
    use vfs::VfsPath;
    use vfs_tools::setup_files;

    use super::{Document, MetaResult};
    use im::ordset;

    #[test]
    fn extract_meta_data() -> MetaResult<()> {
        let root = setup_files! {
            root/"index.md": b"# A heading"
        }?;

        let meta_data = super::extract_meta(&root)?;
        let root: VfsPath = root.into();
        assert_eq!(
            meta_data.documents,
            ordset![Document(super::Path("/root/index.md".to_string()))]
        );
        Result::Ok(())
    }
}
