mod meta;

use vfs::VfsPath;

pub struct Carniti {
    root: VfsPath,
}

impl Carniti {
    pub fn new(fs: VfsPath) -> Self {
        Self { root: fs }
    }
}
