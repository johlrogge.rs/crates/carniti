# Carniti - A wiki engine

Wiki is a hawaiian word that means "quick". With a background
in boat racing I named my Wiki engine Carniti after the italian
engine manufactutrer that along with Selva stood out as extreme
boat engines back in the days when I was active.

## Purpose

Selva is written as part of my dotfiles manager _parchment_.
While extreme and fast may seem like goals it is somewhat
misleading. I am mostly interested in a wiki engine that can be 
queried and mined.
It is supposed to be a personal filesystem based wiki with a 
transparent source of text model, what that means is that it
should be obvioius whic file a text originates from, preferably 
even line and column for tight text editor integration.

